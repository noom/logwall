use hyper::{Body, Request};

use base64::decode;

use std::{error, fmt};

pub type CredentialsResult<T> = std::result::Result<T, CredentialsError>;

#[derive(Debug, Clone)]
pub enum CredentialsError {
    WrongCredentials,
    MalformedCredentials,
    Unauthorized,
}

impl fmt::Display for CredentialsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Couldn't parse credentials")
    }
}

impl error::Error for CredentialsError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

impl From<base64::DecodeError> for CredentialsError {
    fn from(_: base64::DecodeError) -> Self {
        Self::MalformedCredentials
    }
}

impl From<std::str::Utf8Error> for CredentialsError {
    fn from(_: std::str::Utf8Error) -> Self {
        Self::MalformedCredentials
    }
}

pub fn parse_credentials(creds: impl Into<String>) -> CredentialsResult<String> {
    let creds = creds.into();
    let creds = creds
        .split_whitespace()
        .last()
        .ok_or(CredentialsError::MalformedCredentials)?;
    let creds = &decode(&creds)?;
    let creds = std::str::from_utf8(creds)?;

    Ok(creds.to_owned())
}

pub fn is_auth(req: &Request<Body>, creds: impl Into<String>) -> bool {
    if let Some(creds_attempt) = req.headers().get("authorization") {
        if let Ok(creds_attempt) = parse_credentials(creds_attempt.to_str().unwrap()) {
            if creds_attempt == creds.into() {
                return true;
            } else {
                return false;
            }
        }
    }

    false
}
