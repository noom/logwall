use tokio::fs::File;
use tokio::io::AsyncReadExt;

use hyper::{
    service::{make_service_fn, service_fn},
    {Body, Method, Request, Response, Result, Server, StatusCode},
};

use log::{info, warn};

use std::{convert::Infallible, env};

mod auth;

const DEFAULT_LOG_PATH: &str = "log";
const DEFAULT_PORT: &str = "8000";
const DEFAULT_CREDS: &str = "soir:wtf";

/// TODO
/// - Log connection attempts and integrate with fail2ban
/// - Allow multiple files
/// - Reverse file order?
/// - Random messages
/// - Nice interface (wasm frontend?)

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let port = if let Ok(port) = env::var("PORT") {
        info!("PORT is set to {}.", port);
        port
    } else {
        warn!("PORT isn't set, defaults to {}.", DEFAULT_PORT);
        DEFAULT_PORT.into()
    };

    let log_path = if let Ok(log_path) = env::var("LOG_PATH") {
        info!("LOG_PATH is set to {}.", log_path);
        log_path
    } else {
        warn!("LOG_PATH isn't set, defaults to {}.", DEFAULT_LOG_PATH);
        DEFAULT_LOG_PATH.into()
    };

    let creds = if let Ok(creds) = env::var("CREDS") {
        creds
    } else {
        warn!("CREDS isn't set, defaults to {}.", DEFAULT_CREDS);
        DEFAULT_CREDS.into()
    };

    let make_service = make_service_fn(move |_| {
        let log_path = log_path.clone();
        let creds = creds.clone();

        async move {
            Ok::<_, Infallible>(service_fn(move |req| {
                router(req, log_path.clone(), creds.clone())
            }))
        }
    });

    let addr = format!("0.0.0.0:{}", port).parse().unwrap();
    let server = Server::bind(&addr).serve(make_service);

    println!("Listening on {}", addr);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

async fn router(
    req: Request<Body>,
    log_path: impl Into<String>,
    creds: impl Into<String>,
) -> Result<Response<Body>> {
    match (auth::is_auth(&req, creds), req.method(), req.uri().path()) {
        (false, _, _) => Ok(pls_auth()),
        (true, &Method::GET, "/") => send_file(log_path).await,
        _ => Ok(not_found()),
    }
}

/// HTTP status code 401
fn pls_auth() -> Response<Body> {
    Response::builder()
        .status(401)
        .header(
            "WWW-Authenticate",
            format!(
                r#"Basic realm="{}", charset="UTF-8""#,
                "Give it to me, Dave."
            ),
        )
        .body("Just what do you think you're doing, Dave?".into())
        .unwrap()
}

/// HTTP status code 404
fn not_found() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("What are you looking for exactly, Dave?".into())
        .unwrap()
}

/// HTTP status code 500
fn internal_server_error() -> Response<Body> {
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body("Something's broken in there".into())
        .unwrap()
}

async fn send_file(filename: impl Into<String>) -> Result<Response<Body>> {
    if let Ok(mut file) = File::open(filename.into()).await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            return Ok(Response::new(buf.into()));
        }

        return Ok(internal_server_error());
    }

    Ok(not_found())
}
